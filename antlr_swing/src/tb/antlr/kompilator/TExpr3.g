tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr returns [Integer out]   : ^(PLUS  e1=expr e2=expr) -> dodaj(exprL={$e1.st},exprR={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(exprL={$e1.st},exprR={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(exprL={$e1.st},exprR={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(exprL={$e1.st},exprR={$e2.st})
        | ^(PODST i1=ID   e2=expr) {podstaw($ID.text, $e2.out);} -> podstaw(variable={$ID.text}, operand={$e2.st})               
        | INT                      -> int(operand={$INT.text})
        | ID                       {czytaj($ID.text);} -> czytaj(variable={$ID.text})        
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}