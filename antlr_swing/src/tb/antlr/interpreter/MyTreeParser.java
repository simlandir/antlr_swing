package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

    protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer dodaj(Integer operandL, Integer operandR) {
		return operandL + operandR;
	}
	
	protected Integer odejmij(Integer operandL, Integer operandR) {
		return operandL - operandR;
	}
	
	protected Integer pomnoz(Integer operandL, Integer operandR) {
		return operandL * operandR;
	}
	
	protected Integer podziel(Integer operandL, Integer operandR) {
		return operandL / operandR;
	}
	
	protected Integer podstaw(String variableName, Integer value) {
		if(!globals.hasSymbol(variableName)) {
			throw new IllegalStateException(String.format("Zmienna %s nie jest zadeklarowana! Proszę zadeklaruj zmienną %s: 'var %s'!", variableName, variableName, variableName));				
		}
		globals.setSymbol(variableName, value);
		return value;
	}
	
	protected Integer czytaj(String variableName) {
		if(!globals.hasSymbol(variableName)) {
			throw new IllegalStateException(String.format("Zmienna %s nie jest zadeklarowana! Proszę zadeklaruj zmienną %s: 'var %s'!", variableName, variableName, variableName));				
		}
		return globals.getSymbol(variableName);
	}
}
